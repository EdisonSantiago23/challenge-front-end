import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

export class Services {

  protected urlExternal = environment.baseUrlExternal;
  protected urlExternalApi = environment.baseUrlApi;
  
  protected component: string;
  protected http: HttpClient;
  protected httpOptions: any;
  protected httpOptionsFile: any;
  protected authBasic: boolean;
  protected storage: any;

  constructor(component: string, http: HttpClient, authBasic: boolean = false) {
    this.component = component;
    this.http = http;
    this.authBasic = authBasic;
    this.resolvedSchema();
  }

  resolvedSchema(currentSchema: string = null, contentType: string = 'application/json') {
    let schema = currentSchema;
    let authorization = 'Bearer ';
    if (this.authBasic) {
      authorization = 'Basic ';
    }

    this.httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        Authorization: authorization,
        'schema-db': schema,
      })
    };
    this.httpOptions.headers.set('Content-Type', contentType);
  }
  observeResponse() {
    this.httpOptions.observe = 'response';
  }

  get(): Observable<any> {
    return this.http.get(this.urlExternal + 'search?q=watches&token=929dfb0ac94c3fc9e134143671c017bf');
  }
  getPlus(): Observable<any> {
    this.resolvedSchema();
    return this.http.get(this.urlExternalApi + 'weatherforecast');
  }
  
}
