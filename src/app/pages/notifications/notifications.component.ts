import { Component } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { Subject } from 'rxjs';
import {RemoteplusService} from '../../service/remoteplus/remoteplus.service'


@Component({
    selector: 'notifications-cmp',
    moduleId: module.id,
    templateUrl: 'notifications.component.html'
})

export class NotificationsComponent{
  public dataremoteplus:[];

  public destroySubscription$ = new Subject();

  constructor(private remoteplusService: RemoteplusService) {}

  ngOnInit() {
    this.remoteplusService.getRemote(this);

}
ngOnDestroy(): void {
    this.destroySubscription$.next();     // trigger the unsubscribe
    this.destroySubscription$.complete(); // finalize & clean up the subject stream
  }
}
