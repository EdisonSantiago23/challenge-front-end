import { Component, OnInit } from '@angular/core';
import { LocalService } from '../../service/local/local.service';
import { ToastrService } from "ngx-toastr";

@Component({
    selector: 'icons-cmp',
    moduleId: module.id,
    templateUrl: 'icons.component.html'
})

export class IconsComponent implements OnInit {
    public watches: any;
    public showwatches = false;
    constructor(public localService: LocalService, private toastr: ToastrService) { }
    ngOnInit(): void {
        this.getItems();

    }
    getItems(): void {
        this.localService.get().then(res => {
            const data: any = res;
            this.watches = data

        });
    }
    closeModal(close) {
        this.showwatches = close;
        this.getItems();

    }
    showNotification(from, align) {
        this.toastr.error(
            '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">successfully removed          .</span>',
            "",
            {
                timeOut: 4000,
                closeButton: true,
                enableHtml: true,
                toastClass: "alert alert-error alert-with-icon",
                positionClass: "toast-" + from + "-" + align
            }
        );
    }
    newWatches() {
        this.showwatches = true;

    }
    eliminar(watchesid: any): void {
        if (confirm("Want to delete")) {
            this.showNotification('bottom', 'right');

            this.localService.delete(watchesid);
            this.getItems();
        }
    }
}
