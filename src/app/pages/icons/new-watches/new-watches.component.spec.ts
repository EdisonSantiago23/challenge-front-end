import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWatchesComponent } from './new-watches.component';

describe('NewWatchesComponent', () => {
  let component: NewWatchesComponent;
  let fixture: ComponentFixture<NewWatchesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewWatchesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
