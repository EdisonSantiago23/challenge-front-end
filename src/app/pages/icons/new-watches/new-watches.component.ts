import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Watches } from 'app/domain/watches';
import { LocalService } from '../../../service/local/local.service';
import { Guid } from 'guid-typescript';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-new-watches',
  templateUrl: './new-watches.component.html',
  styleUrls: ['./new-watches.component.css']
})
export class NewWatchesComponent implements OnInit {
  @Output() closeModal = new EventEmitter<boolean>();

  public newWatches: Watches;
  public title: string = '';
  public content: string = '';
  public showWatches = false;

  constructor(public localService: LocalService, public toastr: ToastrService) { }

  ngOnInit(): void {
    this.newWatches = new Watches('', '', '', '');
    this.newWatches.id = Guid.create().toString();
    this.newWatches.title = this.title;
    this.newWatches.content = this.content;
    this.showWatches = true;

  }
  guardar() {
    this.localService.insert(this.newWatches)
      .then(res => {
        this.showNotification('bottom', 'right');
        this.showWatches = false;
        this.closeModal.emit(false);
      });
  }
  close() {
    this.showWatches = false;
    this.closeModal.emit(false);
  }
  showNotification(from, align) {
    this.toastr.success(
      '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">success to save.</span>',
      "",
      {
        timeOut: 4000,
        closeButton: true,
        enableHtml: true,
        toastClass: "alert alert-success alert-with-icon",
        positionClass: "toast-" + from + "-" + align
      }
    );
  }
}
