import { Component,OnDestroy,OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {RemoteService} from '../../service/remote/remote.service'

@Component({
    moduleId: module.id,
    selector: 'maps-cmp',
    templateUrl: 'maps.component.html'
})

export class MapsComponent implements OnInit, OnDestroy {
    public dataremote:[];
    public destroySubscription$ = new Subject();


    constructor(public remoteService: RemoteService) { }

    ngOnInit() {
        this.remoteService.getRemote(this);
    
    }
    ngOnDestroy(): void {
        this.destroySubscription$.next();     // trigger the unsubscribe
        this.destroySubscription$.complete(); // finalize & clean up the subject stream
      }
    
}
