import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import { LocalService } from '../../service/local/local.service';
import { RemoteplusService } from '../../service/remoteplus/remoteplus.service'
import { RemoteService } from '../../service/remote/remote.service'


@Component({
  selector: 'dashboard-cmp',
  moduleId: module.id,
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {

  public canvas: any;
  public ctx;
  public chartColor;
  public chartEmail;
  public watches: any;
  public totalwatches: number;
  public dataremote:[];

  constructor(public localService: LocalService, public remoteplusService: RemoteplusService, public remoteService: RemoteService) { }

  ngOnInit(): void {
    //this.remoteService.getRemote(this);

    this.getItems();
    this.chartColor = "#FFFFFF";
    this.canvas = document.getElementById("chartEmail");
    this.ctx = this.canvas.getContext("2d");
    this.chartEmail = new Chart(this.ctx, {
      type: 'pie',
      data: {
        labels: [1, 2, 3],
        datasets: [{
          label: "Emails",
          pointRadius: 0,
          pointHoverRadius: 0,
          backgroundColor: [
            '#e3e3e3',
            '#4acccd',
            '#fcc468',
          ],
          borderWidth: 0,
          data: [4, 1, 2]
        }]
      },

      options: {

        legend: {
          display: false
        },

        pieceLabel: {
          render: 'percentage',
          fontColor: ['white'],
          precision: 2
        },

        tooltips: {
          enabled: false
        },

        scales: {
          yAxes: [{

            ticks: {
              display: false
            },
            gridLines: {
              drawBorder: false,
              zeroLineColor: "transparent",
              color: 'rgba(255,255,255,0.05)'
            }

          }],

          xAxes: [{
            barPercentage: 1.6,
            gridLines: {
              drawBorder: false,
              color: 'rgba(255,255,255,0.1)',
              zeroLineColor: "transparent"
            },
            ticks: {
              display: false,
            }
          }]
        },
      }
    });
  }
  getItems(): void {
    this.localService.get().then(res => {
      const data: any = res;
      this.watches = data;
      this.totalwatches = 2000

    });
  }
  
  
}
