import { Injectable } from '@angular/core';
import { WatchesDatabase } from '../../domain/WatchesDatabase';

@Injectable({
  providedIn: 'root'
})
export class LocalService {
  private localDb: any;

  constructor() { this.localDb = new WatchesDatabase() }

  insert(watche: any) {
    return this.localDb.watches.add(watche);
  }
  delete(id: any) {
    this.localDb.watches.delete(id);
  }
  get() {
    return this.localDb.watches.toArray();
  }
}
