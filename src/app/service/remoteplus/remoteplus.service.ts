import { Injectable } from '@angular/core';
import { Services } from '../../classes/Services';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RemoteplusService extends Services {

  constructor(http: HttpClient) {
    super('', http);
  }
  getRemote = (ctx) => {
    this.getPlus().pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        ctx.dataremoteplus = response?.articles;
      },
      (error) => {
        ctx.dataremoteplus =[];

       console.log(error)
      },
      () => {
      });
  }
}
