import { Injectable } from '@angular/core';
import { Services } from '../../classes/Services';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RemoteService extends Services {

  constructor(http: HttpClient) {
    super('', http);
  }

  getRemote = (ctx) => {
    this.get().pipe(takeUntil(ctx.destroySubscription$)).subscribe(
      (response) => {
        ctx.dataremote = response?.articles;
      },
      (error) => {
        ctx.dataremoteplus =[];
      },
      () => {
      });
  }

}
