import Dexie from 'dexie';

export class WatchesDatabase extends Dexie {
    constructor() {
        super('LocalDatabase');
        this.version(1).stores({
            watches: 'id,title,content,image',
        });
    }
}
